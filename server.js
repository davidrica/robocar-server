const path = require( 'path' );
const express = require( 'express' );
const socketIO = require( 'socket.io' );

// import control API
const { updateGpioState, getInitialState } = require( './control' );

// create an express app
const app = express();
var cors = require('cors');
app.use(cors());

// send `index.html` from the current directory
// when `http://<ip>:8291/` route is accessed using `GET` method
app.get( '/', ( request, response ) => {
  response.sendFile( path.resolve( __dirname, 'web-app/index.html' ), {
    headers: {
      'Content-Type': 'text/html',
    }
  } );
} );

// send asset files
app.use( '/assets/', express.static( path.resolve( __dirname, 'web-app' ) ) );
app.use( '/assets/', express.static( path.resolve( __dirname, 'node_modules/socket.io-client/dist' ) ) );

// server listens on `8291` port
const server = app.listen( 8291, () => console.log( 'Express server started!' ) );

// create a WebSocket server
const io = socketIO( server );

// listen for connection
io.on( 'connection', ( client ) => {
  console.log( 'SOCKET: ', 'A client connected', client.id );
  getInitialState(client);

  // listen to `update-gpio-state` event
  client.on( 'update-gpio-state', ( data ) => {
    console.log('Received update-gpio-state event.' );
    updateGpioState( client, data.r, data.g, data.b, data.v ); // Set GPIO state from client
  } );

  client.on('disconnecting', (reason) => {
    console.log(`Client disconnected (${reason})`);
    updateGpioState( client, false, false, true, 0 ); // Set throtle to 0 if client disconnect
  });

} );
