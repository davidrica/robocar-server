const Gpio = require('pigpio').Gpio;

// configure GPIO pins
const pin_red_pwm = new Gpio(26, { mode: Gpio.OUTPUT });
const pin_green = new Gpio(19, { mode: Gpio.OUTPUT });
const pin_blue = new Gpio(13, { mode: Gpio.OUTPUT });

let pinState = {
  red: false,
  green: false,
  blue: true,
  vertical: 0
};

// Update GPIO states
exports.updateGpioState = (client, r, g, b, v) => {
  if (pinState.vertical != v) {
    pinState.vertical = v;

    // let dutyCycle = 0;

    // setInterval(() => {
    //   pin_red_pwm.pwmWrite(dutyCycle);

    //   dutyCycle += 5;
    //   if (dutyCycle > 255) {
    //     dutyCycle = 0;
    //   }
    // }, 20);

    console.log('pinState: ', pinState)
    pin_red_pwm.pwmWrite(v);
    client.emit('update-gpio-state', pinState);
  }

  if (pinState.green != g) {
    pinState.green = g;
    pin_green.digitalWrite(g ? 1 : 0, err => {
      if (err) { throw err; }
      client.emit('update-gpio-state', pinState);
    });
  }

  if (pinState.blue != b) {
    pinState.blue = b;
    pin_blue.digitalWrite(b ? 1 : 0, err => {
      if (err) { throw err; }
      client.emit('update-gpio-state', pinState);
    });
  }
};

exports.getInitialState = (client) => {
  let pinState = {
    red: false,
    green: false,
    blue: true, // Forward
    vertical: 0
  };

  pinState.green = pin_green.digitalRead() ? true : false;
  pinState.blue = pin_blue.digitalRead() ? true : false;

  client.emit('update-gpio-state', pinState); //send initial GPIO status to client
}