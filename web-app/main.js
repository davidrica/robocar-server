// get button elements
var button_red = document.getElementById( 'button-red' );
var button_green = document.getElementById( 'button-green' );
var button_blue = document.getElementById( 'button-blue' );

// initial button states
var button_red_state = false;
var button_green_state = false;
var button_blue_state = false;
var button_vertical_state = 0;

// check for active connection
var isConnectionActive = false;

// connect to the Web Socket server
// var connection = io( 'http://proxy65.rt3.io:31606' );
// var connection = io( 'http://robocar.portmap.io:9000' );
var connection = io( 'http://id11.tunnel.my.id:1666' );
// var connection = io( 'http://61.5.79.12:9000' );

// when connection is established 
connection.on( 'connect', (data) => {
  isConnectionActive = true;
} );

connection.on( 'disconnect', () => {
  isConnectionActive = false;
} );

connection.on('update-gpio-state', function (data) { //get pin status from server
  console.log('dataToggle: ', data);
});

// WebSocket event emitter function
var emitEvent = function( event ) {
  if( ! isConnectionActive ) {
    return alert( 'Server connection is closed!' );
  }

  // change button state
  if( event.target.id === 'button-red') { button_red_state = ! button_red_state; }
  if( event.target.id === 'button-green') { button_green_state = ! button_green_state; }
  if( event.target.id === 'button-blue') { button_blue_state = ! button_blue_state; }

  // emit `update-gpio-state` socket event
  connection.emit( 'update-gpio-state', {
    r: button_red_state,
    g: button_green_state,
    b: button_blue_state,
    v: button_vertical_state
  } );
};

// add event listeners on button
button_red.addEventListener( 'click', emitEvent );
button_blue.addEventListener( 'click', emitEvent );
button_green.addEventListener( 'click', emitEvent );